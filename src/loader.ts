import { IRequestWithPromiseOptions, requestWithPromise } from './helpers/request-with-promise.helper';

export class ArmLoader {

	constructor() {
		this.loadConfig()
			.then((config) => {
				console.log('config', config);
			})
	}

	private loadConfig(): Promise<any> {
		const requestOptions: IRequestWithPromiseOptions = {
			method: 'GET',
			url: '//api.jsonbin.io/b/5a8f9f9aa121bc097fe70ae7'
		};

		return requestWithPromise(requestOptions)
	}
}